<?php

namespace App;

use App\TableFilter;

use Auth;
use DB;

class ClienteFilter extends TableFilter
{
    function __construct($table_name, $filters){
        parent::__construct($table_name, $filters);
    }

    public function filter(){
        $this->do_joins();

        foreach($this->filters as $f_name => $f_value){
            if($f_value!==""){
                switch ($f_name) {
                    case 'estado_id':
                        $this->query->where('cliente.estado_id', $f_value);
                        break;
                    case 'created_at':
                        $this->query->whereBetween('cliente.created_at', [$f_value.' 00:00:00', $f_value.' 23:59:59']);
                        break;
                }
            }
        }

        $this->do_select();

        $this->query->orderBy('cliente.created_at', 'desc');

        $user = Auth::user();
        if(strtolower($user->grupo->nombre) == "vendedor"){
            $this->query->where('cliente.users_id', $user->id);
        }
        
        return $this;
    }

    public function do_joins(){
        $this->query->join('users', 'users.id', '=', 'users_id')
                    ->leftJoin('producto', 'producto.id', '=', 'producto_id')
                    ->join('estado', 'estado.id', '=', 'estado_id')
                    ->leftJoin('metodo_pago', 'metodo_pago.id', '=', 'metodo_pago_id')
                    ->leftJoin('localidad', 'localidad.id', '=', 'localidad_id')
                    ->leftJoin('tipo_cliente', 'tipo_cliente.id', '=', 'cliente.tipo_cliente_id');
    }

    public function do_select(){
        $select = [
            'cliente.id as id_cliente',
            "CONCAT(cliente.nombre,' ',cliente.apellido) as nombre_cliente",
            "DATE_FORMAT(cliente.created_at,'%d %b %Y') as fecha_venta",
            'tipo_cliente.nombre as tipo_cliente',
            "CONCAT(ifnull(users.nombre,''),' ',ifnull(users.apellido,'')) as nombre_vendedor",
            'producto.nombre as producto',
            'estado.nombre as estado',
            'metodo_pago.nombre as metodo_pago',
            'localidad.nombre as localidad'
        ];
        $this->query->select(DB::raw(implode(", ", $select)));
    }
}