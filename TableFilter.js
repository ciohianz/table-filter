var tf = TableFilter = function(tf_container, options){
    var defaults = {
        url: '/',
        row_template: '',
        data: {
            page: 1
        },
        // paginator_template: paginator_template,
        rand_paginator_id: ''
    }
    
    extend_tf(this, defaults, options);

    this.container = document.querySelector(tf_container);
    this.timeout = null;

    this.bind_filters();

    this.create_drawers();
}

TableFilter.prototype.create_drawers = function(){
    this.draw_row = new Function('obj', generate_source(this.row_template))
}

TableFilter.prototype.set_filter = function(key, value){
    if(typeof key == "object") {
        for(var sub_key in key){
            this.data[sub_key] = key[sub_key];
        }
    } else {
        this.data[key] = value;
    }

    this.do_request();
}

TableFilter.prototype.bind_filters = function(){
    var _this = this;
    [].forEach.call(this.container.querySelectorAll('[data-tf-filter]'), function(element){
        var events = element.nodeName=="INPUT" ? ['keydown', 'change'] : ['change'];
        if(element.dataset.tfPlugin || element.type=='hidden'){
            events.forEach(function(event){
                _this.bind_filters_$(element, event);
            });
        } else {
            events.forEach(function(event){
                element.addEventListener(event, _this.get_data.bind(_this));
            });
        }
        _this.data[element.dataset.tfFilter] = element.value;
    });
}

TableFilter.prototype.bind_filters_$ = function(element, event){
    $(element).off(event).on(event, this.get_data.bind(this));
}

TableFilter.prototype.get_data = function(e){
    clearTimeout(this.timeout);

    var _tf = this;
    this.timeout = setTimeout(function(){
        var element = e.target;
        _tf.data[element.dataset.tfFilter] = element.value;
        _tf.data.page = 1;
        _tf.do_request();
    }, 500);

    return this;
}

TableFilter.prototype.do_request = function(cb){
    var _this = this;

    var params = this.build_params();
    var xhr = new XMLHttpRequest();
    xhr.open('GET', this.url+params, true);
    xhr.setRequestHeader('Accept', 'application/json');
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.onload = function(e) {
        if (this.status == 200) {
            _this.render_table(JSON.parse(this.response));
            typeof cb == "function" && cb();
        }
    };
    xhr.send();

    return this;
}

TableFilter.prototype.build_params = function(){
    return '?'+JSON.stringify(this.data).replace(/[\"\{\}]/g, '').replace(/\:/g, '=').replace(/\,/g, '&');
}

TableFilter.prototype.render_table = function(data){
    var table = this.container.querySelector("table"),
        tbody = this.container.querySelector("table tbody") || this.container.querySelector("table"),
        _this = this;

    tbody.innerHTML = "";
    data.collection.forEach(function(row){
        tbody.innerHTML += _this.draw_row(row);
    });

    this.render_paginator(table, data)

    return this;
}

TableFilter.prototype.render_paginator = function(table, data) {
    var old_random_id = this.rand_paginator_id;
    if(old_random_id)
        document.getElementById(old_random_id).remove();

    var paginator = document.createElement('nav');
    paginator.id = this.rand_paginator_id = generate_id();
    
    /* Logica paginador */
    var pagination = this.get_pagination(data);

    // paginator.innerHTML = this.draw_paginator(pagination);
    paginator.innerHTML = pagination;
    table.parentNode.insertBefore(paginator, table.nextSibling)

    this.bind_paginator();
}

TableFilter.prototype.get_pagination = function(data) {
    var contenido;

    if (data.lastPage < 13) {
        contenido = this.get_page_range(data, 1, data.lastPage);
    } else {
        contenido = this.get_page_slider(data);
    }

    return this.get_start_nav()+this.get_previous(data)+contenido+this.get_next(data)+this.get_end_nav()+this.get_contador(data);
}

TableFilter.prototype.get_start_nav = function(){
    return '<nav id="nav-paginator" class="pull-right"><ul class="pagination" style="margin-top: 0px;">';
}

TableFilter.prototype.get_end_nav = function(){
    return '</nav></ul>';
}

TableFilter.prototype.get_contador = function(data){
    return '<span class="pull-left">'+data.from+' al '+data.to+' de '+data.total+'</span>'
}

TableFilter.prototype.get_page_range = function(data, start, end){
    var pages = [], page;

    for (page = start; page <= end; page++) {
        if (data.currentPage == page){
            pages.push(this.get_active_page_wrapper(page));
        } else {
            pages.push(this.get_page_wrapper(page));
        }
    }

    return pages.join('');
}

TableFilter.prototype.get_page_wrapper = function(text, page) {
    page || (page=text)
    return '<li data-page="'+page+'"><span>'+text+'</span></li>';
}

TableFilter.prototype.get_disabled_page_wrapper = function(text, page) {
    page || (page=text)
    return '<li data-page="'+page+'" class="disabled"><span>'+text+'</span></li>';
}

TableFilter.prototype.get_active_page_wrapper = function(text, page) {
    page || (page=text)
    return '<li data-page="'+page+'" class="active"><span>'+text+'</span></li>';
}

TableFilter.prototype.get_page_slider = function(data){
    var _window = 6;

    if (data.currentPage <= _window) {

        var ending = this.get_finish(data);
        return this.get_page_range(data, 1, _window + 2)+ending;

    } else if (data.currentPage >= data.lastPage - _window) {

        var start = data.lastPage - 8;
        content = this.get_page_range(data, start, data.lastPage);
        return this.get_start(data)+content;

    } else {

        content = this.get_adjacent_range(data);
        return this.get_start(data)+content+this.get_finish(data);

    }
}

TableFilter.prototype.get_previous = function(data, text){
    text || (text='<i class="fa fa-angle-double-left"></i>');

    if (data.currentPage <= 1) {
        return this.get_disabled_page_wrapper(text, 'previous');
    } else {
        return this.get_page_wrapper(text, 'previous');
    }
}

TableFilter.prototype.get_next = function(data, text){
    text || (text='<i class="fa fa-angle-double-right"></i>');

    if (data.currentPage >= data.lastPage) {
        return this.get_disabled_page_wrapper(text, 'next');
    } else {
        return this.get_page_wrapper(text, 'next');
    }
}

TableFilter.prototype.get_finish = function(data){
    var content = this.get_page_range(data, data.lastPage - 1, data.lastPage);

    return this.get_dots()+content;
}

TableFilter.prototype.get_start = function(data){
    return this.get_page_range(data, 1, 2)+this.get_dots();
}

TableFilter.prototype.get_adjacent_range = function(data){
    return this.get_page_range(data, data.currentPage - 3, data.currentPage + 3);
}

TableFilter.prototype.get_dots = function(){
    return this.get_disabled_page_wrapper("...");
}

TableFilter.prototype.bind_paginator = function(){
    var _this = this;
    var nav_paginator = document.getElementById('nav-paginator');

    var list_items = document.querySelectorAll('#nav-paginator li:not(.disabled)');
    [].forEach.call(list_items, function(li, index){
        li.addEventListener('click', function(e){
            var page = e.currentTarget.dataset.page;

            switch(page){
                case 'next':
                    _this.data.page++;
                    break;
                case 'previous':
                    _this.data.page--;
                    break;
                default:
                    _this.data.page = page;
                    break;
            }

            _this.do_request();
        })
    })
}

function extend_tf(){
    for(var i = arguments.length - 1; i >= 1; i--) {
        for(var key in arguments[i]) {
            arguments[i-1][key] = arguments[i][key]
        }
    }
}

function generate_source(text){
    var matcher = RegExp([
        (templateSettings.interpolate).source,
        (templateSettings.evaluate).source
    ].join('|') + '|$', 'g');
    
    var source = "__p+='";
    var index = 0;
    text.replace(matcher, function(match, interpolate, evaluate, offset, str){
        source += text.slice(index, offset).replace(escaper, escapeChar);
        index = offset + match.length;

        if (interpolate) {
            source += "'+\n((__t=(" + interpolate + "))==null?'':__t)+\n'";
        } else if (evaluate) {
            source += "';\n" + evaluate + "\n__p+='";
        }

        return match;
    });
    source += "';\n";

    source = 'with(obj||{}){\n' + source + '}\n';

    source = "var __t, __p='',__j=Array.prototype.join," +
        "print=function(){__p+=__j.call(arguments,'');};\n" +
        source + 'return __p;\n';

    return source;
}

/*var paginator_template = '<nav id="nav-paginator" class="pull-right">' +
  '<ul style="margin-top: 0px;" class="pagination">' +
    '<li class="%%= (currentPage==1 ? \"disabled\" : \"\") %%" data-page="previous" id="previous">' +
      '<a href="javascript:void(0)" aria-label="Previous">' +
        '<i class="fa fa-angle-double-left"></i>' +
      '</a>' +
    '</li>' +
    '%% for(var __i=currentPage; __i<=lastPage; __i++){ %%' +
        '<li class="%%= (currentPage==__i ? \"active\" : \"\") %%" data-page="%%= __i %%"><a href="javascript:void(0)">%%= __i %%</a></li>' +
    '%% } %%' +
    '<li class="%%= (currentPage==lastPage ? \"disabled\" : \"\") %%" data-page="next" id="next">' +
      '<a href="javascript:void(0)" aria-label="Next">' +
        '<i class="fa fa-angle-double-right"></i>' +
      '</a>' +
    '</li>' +
  '</ul>' +
'</nav>' +
'<span class="pull-left">' +
    '%%= from %% al %%= to %% de %%= total %%' +
'</span>';*/

function generate_id(){
    var text = "tf-";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < 5; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}


/* REGEX STUFF */

var templateSettings = {
    evaluate: /%%([\s\S]+?)%%/g,
    interpolate: /%%=([\s\S]+?)%%/g,
};


var escapes = {
    "'":      "'",
    '\\':     '\\',
    '\r':     'r',
    '\n':     'n',
    '\u2028': 'u2028',
    '\u2029': 'u2029'
  };

var escaper = /\\|'|\r|\n|\u2028|\u2029/g;


var escapeChar = function(match) {
    return '\\' + escapes[match];
};