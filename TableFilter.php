<?php

namespace App;

use DB;

class TableFilter
{
    public function __construct($table_name, $filters){
        $this->query = DB::table($table_name);
        $this->filters = $filters;

        $this->per_page = array_key_exists("per_page", $this->filters) ? $this->filters['per_page'] : 10;

    }

    public function get(){
        $paginator = $this->query->paginate($this->per_page);
        return [
            'collection' => $paginator->getCollection(),
            'from' => ($from=$paginator->firstItem()) ? $from : 0,
            'to' => ($to=$paginator->lastItem()) ? $to : 0,
            'total' => $paginator->total(),
            'lastPage' => $paginator->lastPage(),
            'currentPage' => $paginator->currentPage()
        ];
    }

    public function getQueryObject(){
        return $this->query;
    }
}

?>